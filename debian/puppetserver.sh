#!/usr/bin/sh -e

case $1 in
	ca)
		shift
		/usr/share/puppetserver/ca.rb "$@"
	;;

	*)
		# shellcheck disable=SC2086
		/usr/bin/java $JAVA_OPTS --add-opens java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.base/java.io=ALL-UNNAMED -jar /usr/share/java/puppetserver.jar --bootstrap-config "/etc/puppetserver/services.d/,/usr/share/puppetserver/bootstrap.cfg" --config /etc/puppetserver/conf.d "$@"
	;;
esac
